const cheerio = require('cheerio');
const axios = require('axios');
import './style.css';

var block = document.getElementById("infoblock");
var btn = document.getElementById("gotoURLButton");
var output = document.getElementById("out");
var outtitles = document.getElementById("outtitles");
let pop_up = document.getElementById("popup");
let err_form = document.getElementById("err_form");

let proxy = "https://cors-anywhere.herokuapp.com/"
 btn.onclick =  function (){
    pop_up.style="display: block"
    var url = document.getElementById('inputurl').value;
    //console.log(url);
    try{     
        axios.get(url,{
            headers: {
                //'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36',
                //'x-apikey': '59a7ad19f5a9fa0808f11931',
                'Access-Control-Allow-Origin' : '*',
                withCredentials: true,
                crossorigin:true,
                'Access-Control-Allow-Credentials':true
                //'Access-Control-Allow-Credentials': true,
                //'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
        }).then(html =>{
            const $ = cheerio.load(html.data)
            let text = ''
            var title = $('head>title').text();
            $('a').each((i, elem) =>{
                text+= `${$(elem).text()}\n`
            });
            output.innerHTML = title;
            outtitles.innerHTML = text;
            block.style = "display: block;"
            err_form.style = "display: none;" 
        } )
        .catch(err => {
            if (err.response) { 
                // client received an error response (5xx, 4xx)
                err_form.innerHTML="Ошибка ввода либо страница не существует"
                
              } else if (err.request) { 
                // client never received a response, or request never left 
                err_form.innerHTML="Возможно, программная ошибка"
              } else { 
                // anything else 
                err_form.innerHTML="Неизвестная ошибка"

              }
              block.style = "display: none;"
              err_form.style = "display: block;" 
        })
        setTimeout(() => pop_up.style="display: none", 2000);
        //pop_up.style="display: none"
        
        
    }
    catch(error){
        block.innerHTML="Ошибка"
    }
    
}
