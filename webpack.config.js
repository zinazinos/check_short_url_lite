const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//const fileloader = require('file-loader')
let mode = 'development'
if (process.env.NODE_ENV === 'production'){
    mode = 'production'
}
console.log(mode+' mode')
module.exports =  {
    mode: mode,
    
    plugins: [
        new MiniCssExtractPlugin(),
        new HtmlWebpackPlugin({
        template: "./src/index.html"
    })],
    module:{
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use:[
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                ]
            },
            {
                test: /\.(jpg|png|svg)$/,
                loader: 'file-loader',
                options: {
                    name: 'images/[name].[ext]'
                }
            },
        ],
    },
}